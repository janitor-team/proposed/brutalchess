brutalchess (0.5.2+dfsg-8) unstable; urgency=medium

  * Team upload.
  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.2.1.
  * Improve find delete command in debina/rules.
  * Remove deprecated menu file.
  * Install brutalchess.png into 32x32 hicolor icon directory.
  * Move the package to salsa.debian.org.
  * Use pkg-config instead of freetype-config.
    Thanks to Reiner Herrmann for the patch. (Closes: #892337)

 -- Markus Koschany <apo@debian.org>  Sat, 20 Oct 2018 18:57:34 +0200

brutalchess (0.5.2+dfsg-7) unstable; urgency=medium

  * Team upload.
  * Drop FreeType.patch. Apparently src:freetype has reverted the changes from
    earlier versions and the path to freetype.h had to be adjusted again.
    Thanks to Chris West for the report. (Closes: #808488)

 -- Markus Koschany <apo@debian.org>  Mon, 21 Dec 2015 00:43:32 +0100

brutalchess (0.5.2+dfsg-6) unstable; urgency=medium

  * Vcs-Browser: Use https.
  * Update 05_use_our_fonts.diff: Use the correct path to DejaVuSans.ttf.
    Thanks to Chris Leick for the report. See also #791671.
  * Declare compliance with Debian Policy 3.9.6.
  * wrap-and-sort -sa.

 -- Markus Koschany <apo@debian.org>  Fri, 30 Oct 2015 17:41:17 +0100

brutalchess (0.5.2+dfsg-5) unstable; urgency=medium

  [ Markus Koschany ]
  * Team upload.
  * Add FreeType.patch. Update fontloader.h to detect the latest version of
    FreeType2 and make the software compile again. (Closes: #732235)
  * Bump Standards-Version to 3.9.5, no changes.
  * Add keywords to desktop file.
  * debian/control: Depend on fonts-dejavu instead of ttf-dejavu.
  * Add disable-quake-pieces.patch. The -p quake option is useless and causes a
    segmentation fault since upstream never shipped the quake models because of
    copyright concerns. (Closes: #732227)

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org.

 -- Markus Koschany <apo@gambaru.de>  Wed, 01 Jan 2014 18:40:01 +0100

brutalchess (0.5.2+dfsg-4) unstable; urgency=low

  [ Iain Lane ]
  * debian/watch: Mangle version correctly

  [ Vincent Legout ]
  * Fix ftbfs with gcc-4.7, add patches/06_gcc-4.7.patch (Closes: #667121)
  * debhelper 9 (debian/compat, debhelper Build-Depends)
  * debian/control:
    - Drop quilt, automake1.9 and libtool in Build-Depends. Add dh-autoreconf.
    - Add myself as uploader
    - Fix Vcs-Browser field
    - Update short and long descriptions
    - Standards-Version 3.9.3
  * debian/dirs: Drop, not needed.
  * Use 3.0 (quilt) source format (Add debian/source/format).
  * debian/rules: Use dh()
    - Enable hardening build flags
    - Add debian/brutalchess.install
  * debian/copyright: Explicitly refer to GPL-2

 -- Vincent Legout <vlegout@debian.org>  Sun, 15 Apr 2012 11:29:50 +0200

brutalchess (0.5.2+dfsg-3) unstable; urgency=low

  [ Barry deFreese ]
  * New maintainer. (Closes: #461885).
    + Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
  * Add watch file.
  * Add VCS tags.
  * Remove deprecated encoding tag from desktop file.
  * Move copying in config.{sub,guess} to configure target.
    + Remove them on clean. (Closes: #482724).
  * Replace xlibmesa-gl-dev build-dep with libgl1-mesa-dev.
  * Bump Standards Version to 3.8.0. (No changes needed).

 -- Barry deFreese <bddebian@comcast.net>  Tue, 17 Jun 2008 15:07:23 -0400

brutalchess (0.5.2+dfsg-2) unstable; urgency=low

  [Fathi Boudra]
  * Add patch to load DejavVu fonts and fix datadir. (Closes: #468925)
  * Drop ttf-bitstream-vera dependency. (Closes: #461262)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sun, 16 Mar 2008 10:40:33 +0100

brutalchess (0.5.2+dfsg-1) unstable; urgency=low

  * New upstream version. (Closes: #427341)

  [Fathi Boudra]
  * Add alternative dependency on ttf-dejavu. (Closes: #439236)
  * Add patch to fix ftbfs with gcc-4.2. (Closes: #409486)
  * Add patch to fix ftbfs with gcc-4.3. (Closes: #413483)
  * Merge desktop file from Ubuntu.
  * Use quilt patch system.
  * Add quilt, automake1.9, libtool, libxmu-dev and libxi-dev build
    dependencies.
  * Use Homepage field.
  * Bump Standard-Version to 3.7.3.
  * Build with --no-undefined and --as-needed linker flags.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sat, 12 Jan 2008 11:38:32 +0100

brutalchess (0.5+dfsg-1) unstable; urgency=low

  * New upstream version.
  * Bump standards version.
  * Rebuilt the tarball (non-free fonts).

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Mon, 25 Dec 2006 18:56:01 +0100

brutalchess (0.0.20060314cvs-2) unstable; urgency=low

  * Add images with alpha (edited in Gimp). (Closes: #360523)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Fri,  7 Apr 2006 10:41:25 +0200

brutalchess (0.0.20060314cvs-1) unstable; urgency=low

  * Initial release. (Closes: #356991)
    - The source package includes one Bitstrem Vera font.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Tue, 14 Mar 2006 23:03:22 +0100
